package com.example.myapplication;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editEmailAddress, editPassword;
    private AppCompatButton btnLogin,btnSignup;
    private TextView txtForgot, txtWelcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_loyout);
        findView();
    }

    private void findView() {
        editEmailAddress = findViewById(R.id.editEmailAddress);
        editPassword = findViewById(R.id.editPassword);
        btnLogin = findViewById(R.id.btnLogin);
        txtForgot = findViewById(R.id.txtForgot);
//        btnSignup = findViewById(R.id.btnSignup);
        btnLogin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

//
//        btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!editEmailAddress.getText().toString().isEmpty() && !editPassword.getText().toString().isEmpty())
//                    ;
//            }
//
//        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnLogin) {

        } else if (view.getId() == R.id.btnSignup) {

            Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
            intent.putExtra("Email", editEmailAddress.getText().toString().trim());
//            intent.putExtra("Email","Email Address");
            startActivity(intent);
            finish();
        }
    }

    ActivityResultLauncher<Intent> signupActivityLauncher=registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result){
                    if(result.getResultCode()== Activity.RESULT_OK){
                        // There are no request codes;
                        Intent data=result.getData();
                        Toast.makeText(LoginActivity.this,data.getStringExtra("Result"),Toast.LENGTH_SHORT).show();
                    }
                }
            }
    );

    @Override
    protected void onActivityResult(int requestCode, int resultCode ,@Nullable Intent data) {

        if (requestCode == 100 && resultCode == RESULT_OK) {
            Toast.makeText(this, data.getStringExtra("Result"), Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);


    }

}