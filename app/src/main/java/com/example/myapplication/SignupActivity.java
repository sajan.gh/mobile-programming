package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {
    String email="";
    private AppCompatButton btnSignup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_layout);
        email=getIntent().getStringExtra("Email");
        Toast.makeText(this,email,Toast.LENGTH_SHORT).show();
        Log.d("Email Address",email);
        btnSignup.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View view){

                Intent intent =new Intent();
                intent.putExtra("Result","Result Return");
                setResult(RESULT_OK,intent);
                finish();
            }

        });
    }

}